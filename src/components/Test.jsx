import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import PropTypes from 'prop-types';
import { getUsers, addUser, getUser, updateUser } from '../actions/Default';
import { connect } from 'react-redux';

class test extends Component {

  constructor(props) {
    super(props);
    this.updateField = this.updateField.bind(this);
    // this.getUsers = this.props.getUsers.bind(this);
    this.doSomething = this.props.doSomething.bind(this);
    this.getUser = () => this.props.getUser(2);
    this.addUser = () => this.props.addUser({
      name: this.state.name,
      email: this.state.email
    });
    this.state = {};
  }

  componentDidMount () {
    this.updateUser = () => this.props.updateUser(this.props.users[0]._links.update.href,
      { horses: true,
      cheese: 'hello' });
  }

  updateField(event, name) {
    const temp = {};
    temp[name] = event.target.value;
    this.setState({ ...temp });
  }

  doSomething() {
    this.props.doSomething('hello world');
  }

  render() {
    return (
      <div>
        <Button onClick={this.updateUser} disabled={this.props.loading} variant="contained" color="primary">Update User</Button>
        <Button onClick={this.getUser} disabled={this.props.loading} variant="contained" color="primary">Get User 2</Button>
        {this.props.result.map((user, key) => (
          <div key={key}>
            <div>Your name: {user.name}</div>
            <div>Your email: {user.email}</div>
          </div>
        ))}

        {this.props.error &&
          <div>{this.props.error}</div>
        }
        <div style={{
          display: 'flex',
          justifyContent: 'space-around',
          width: '30%',
          margin: '10px'
        }} >
          <TextField label="Name" variant="outlined" onChange={ev => this.updateField(ev, 'name')} />
          <TextField label="Email Address" variant="outlined" onChange={ev => this.updateField(ev, 'email')} />
        </div>
        <Button onClick={this.addUser} variant="outlined" color="secondary">Add {this.state.name || 'Empty User'}</Button>
        <div>{this.state.number}</div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  error: state.Simple.error || 'No Errors so far!',
  example: state.Simple.example || 'example not set',
  loading: state.Simple.status === 'started',
  result: state.Simple.result || [],
  users: state.Simple.user
});

test.propTypes = {
  addUser: PropTypes.func,
  error: PropTypes.string,
  example: PropTypes.string,
  getUser: PropTypes.func,
  updateUser: PropTypes.func,
  getUsers: PropTypes.func,
  loading: PropTypes.bool,
  result: PropTypes.array,
  users: PropTypes.array,
  userToAdd: PropTypes.object,
  update: PropTypes.string
};

export default connect(mapStateToProps,
  {
    addUser,
    getUser,
    getUsers,
    updateUser
  })(test);