import Button from '@material-ui/core/Button';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import './common.css';

class Skills extends Component {
  constructor(props) {
    super(props);
    this.selectSkill = this.selectSkill.bind(this);
    this.state = { list: props.list || [] };
    this.orig = props.list || [];
  }

  selectSkill(item, index) {
    console.log(this.props.list);
    item.selected = !item.selected;
    this.props.list[index] = item;
    this.setState({
      list: this.props.list
    });
    this.props.setThem(this.props.list);
  }

  render() {
    return (
      <div className="item-container">
        {this.state.list.map((x, i) => <Button className="item" variant={x.selected ? 'contained' : 'text'} color={x.selected ? 'primary' : 'secondary'} onClick={() => this.selectSkill(x, i)} key={x.name}>{x.name}</Button>)}
      </div>
    );
  }
}

export default connect()(Skills);
