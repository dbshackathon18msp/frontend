import React, { Component } from 'react';
import RouterRoot from '../RouterRoot';
import { MuiThemeProvider } from '@material-ui/core';
import CustomTheme from '../theme/theme';

class App extends Component {
  render() {
    return (
      <MuiThemeProvider theme={CustomTheme} >
        <RouterRoot></RouterRoot>
      </MuiThemeProvider>
    );
  }
}

export default App;