import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { getUsers } from '../actions/Default';

export class Users extends Component {

  constructor(props) {
    super(props);
    console.log('users props', props)
  }

  componentDidMount () {
    console.log('did mount', this.props)
    this.props.getUsers();
  }

  render() {
    console.log('rendering', this.props)
    return (
      <div>
        {this.props.users && JSON.stringify(this.props.users[0])}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  users: state.Simple.allUsers
});

Users.propTypes = {
  users: PropTypes.array,
  getUsers: PropTypes.func
};

export default connect(mapStateToProps,
  { getUsers })(Users);
