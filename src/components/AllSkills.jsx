import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { getSkills } from '../actions/Default';

export class AllSkills extends Component {

  constructor(props) {
    super(props);
    console.log('skills props', props);
  }

  componentDidMount() {
    console.log('did mount', this.props);
    this.props.getSkills();
  }

  render() {
    console.log('rendering', this.props);

    return (
      <div>
        {this.props.skills && JSON.stringify(this.props.skills[0])}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  skills: state.Simple.allSkills
});

AllSkills.propTypes = {
  skills: PropTypes.array,
  getSkills: PropTypes.func
};

export default connect(mapStateToProps,
  { getSkills })(AllSkills);
