import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { selectSkillItem, getSkills, selectWantedItem, updateSkillsAndWishlist, getUserSkills } from '../../actions/Default';
import { connect } from 'react-redux';
import { Checkbox, Button, FormControlLabel, Select } from '@material-ui/core';
import Skills from '../Common/Skills';
import MenuItem from '@material-ui/core/MenuItem';


class CreateUser extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: 0,
      name: ' '
    };
    this.updateField = this.updateField.bind(this);
    this.updateSkillData = this.updateSkillData.bind(this);
    this.updateClientPreference = this.updateClientPreference.bind(this);
  }

  updateClientPreference(ev) {
    this.setState({ engagement: ev.target.value });
  }


  componentDidMount() {
    this.setState({ id: this.props.match.params.id });
    // Api calls here two gets
    this.props.getSkills('wishlist');
    this.props.getUserSkills(this.props.match.params.id);
  }

  updateSkillData() {
    const skillIds = this.props.skills.filter(x => x.selected).map(x => x.id),
      wishlistIds = this.props.wishlist.filter(x => x.selected).map(x => x.id);

    this.props.updateSkillsAndWishlist({
      skills: skillIds,
      wishlist: wishlistIds,
      id: this.state.id
    });
  }

  updateField(event, name) {
    const temp = {};
    temp[name] = event.target.value;
    this.setState({ ...temp });
  }

  render() {

    return (
      <div>
        <h1 style={{ textAlign: 'center' }}>Update User</h1>
        <FormControlLabel
          checked={this.state.mentor}
          control={<Checkbox onChange={() => this.setState({
            mentor: !this.state.mentor
          })} />}
          label="Able to Mentor Others" />
        <br />
        <h3>I know...</h3>
        {this.props.skills && <Skills list={this.props.skills} onSelectItem={this.updateData} setThem={this.props.selectSkillItem} />}
        <h3>I'd like to learn...</h3>
        {this.props.wishlist && <Skills list={this.props.wishlist} onSelectItem={this.updateWishList} setThem={this.props.selectWantedItem} />}
        <br />

        I feel <Select
          value={this.state.engagement}
          onChange={this.updateClientPreference}
          displayEmpty
          name="age"
        >
          <MenuItem value={'Stay'}>😀</MenuItem>
          <MenuItem value={'Neutral'}>¯\_(ツ)_/¯</MenuItem>
          <MenuItem value={'Go'}>😒</MenuItem>
        </Select> on my current engagement.
        <br />
        <br />
        <Button variant="outlined" color="secondary" onClick={this.updateSkillData}>Update</Button>
      </div>
    );
  }
}

CreateUser.propTypes = {
  match: PropTypes.object,
  skills: PropTypes.array,
  wishlist: PropTypes.array,
  selectSkillItem: PropTypes.func,
  selectWantedItem: PropTypes.func,
  updateSkillsAndWishlist: PropTypes.func
};

const mapStateToProps = state => {
  console.log(state);


  return {
    name: state.name,
    list: state.Simple.skills,
    skills: state.Simple.existingSkills,
    wishlist: state.Simple.wishlist
  };
};


export default connect(mapStateToProps, {
  selectSkillItem,
  getSkills,
  selectWantedItem,
  updateSkillsAndWishlist,
  getUserSkills
})(CreateUser);