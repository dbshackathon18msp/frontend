import React, { Component } from 'react';
import Skills from '../Common/Skills';
import { getSkills, getUsersWithData } from '../../actions/Default';
import { connect } from 'react-redux';

class Index extends Component {
  constructor(props) {
    super(props);
    this.state = { users: [] };
  }

  style = {
    container: {
      display: 'flex',
      justifyContent: 'flex-start',
      alignItems: 'center',
      flexWrap: 'wrap'
    },
    single: {
      border: '2px solid black',
      borderRadius: '20px',
      margin: '10px',
      padding: '7px'
    }
  }

  componentDidMount = () => {
    this.props.getSkills('Available');
    this.onSet = this.onSet.bind(this);
    this.props.getUsersWithData();
  };

  onSet(data) {
    const selected = data.filter(x => x.selected);
    this.setState({
      users: this.props.users
    });
    console.log(this.state.users);
  }

  render() {
    return (
      <div>
        What Skills are you looking for?
        {this.props.skills && <Skills list={this.props.skills} setThem={this.onSet}></Skills>}
        <div style={this.style.container}>
          {this.state.users && this.state.users.map((user, idx) => <div key={idx} style={this.style.single}>
            <h3>{user.name}</h3>
            <h5>{user.email}</h5>
          </div>
          )}
        </div>
      </div>
    );
  }
}
export default connect(state => ({
  skills: state.Simple.Available,
  users: state.Simple.allUsersWithData
}), {
    getSkills,
    getUsersWithData
  })(Index);
