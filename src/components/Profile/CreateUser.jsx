import TextField from '@material-ui/core/TextField';
import { Button } from '@material-ui/core';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { addUser } from '../../actions/Default';

class CreateUser extends Component {
  awaitingUpdate = false;

  constructor(props) {
    super(props);
    this.state = {
      email: '',
      name: ''
    };
    this.autogenEmail = this.autogenEmail.bind(this);
    this.update = this.update.bind(this);
    this.addUser = () => {
      this.props.addUser({
        email: this.state.email,
        name: this.state.name,
        role: 'CONSULTANT'
      });
      this.awaitingUpdate = true;
    };
  }

  componentDidUpdate() {
    if (this.props.redirect && this.awaitingUpdate) {
      this.props.history.push('/');
    }
  }


  autogenEmail(ev) {
    this.setState({ email: `${ev.target.value.replace(' ', '.')}@daugherty.com` });
  }

  update(ev, name) {
    const xx = {};
    xx[name] = ev.target.value;
    this.setState(xx);
  }

  render() {
    return (
      <div>
        <h1 style={{ textAlign: 'center' }}>Create User</h1>
        <TextField onChange={ev => this.update(ev, 'name')} variant="outlined" label="Name" onBlur={this.autogenEmail} value={this.state.name} />
        <TextField onChange={ev => this.update(ev, 'email')} variant="outlined" label="Email" value={this.state.email} />
        <br />
        <Button color="primary" onClick={this.addUser}>Create User</Button>
      </div>
    );
  }
}

export default connect(data => ({ redirect: data.Simple.status === 'success' }), {
  addUser
})(CreateUser);