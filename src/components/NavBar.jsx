import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

const styles = {
  root: {
    flexGrow: 1
  }
};


function NavBar() {
  const style = {
    img: {
      width: '22.5vw',
      height: '4.6vw',
      background: 'URL(https://www.daugherty.com/wp-content/uploads/2016/06/daugherty_logo_stacked_4c.jpg)',
      mixBlendMode: 'color-burn',
      backgroundSize: 'contain',
      backgroundRepeat: 'no-repeat'
    },
    toolbar: {
      display: 'flex',
      justifyContent: 'space-between'
    }
  };

  return (
    <div >
      <AppBar position="static" color="primary">
        <Toolbar style={style.toolbar}>
          <Typography variant="h4" color="inherit">
            <a href="/" style={{textDecoration: "none", color: "black"}}>
            Consultant Care
            </a>
          </Typography>
          <Typography variant="h6" color="inherit">
            Grow Your Career While Growing Our Business
          </Typography>
          <div style={style.img} ></div>
        </Toolbar>
      </AppBar>
    </div>
  );
}

NavBar.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(NavBar);