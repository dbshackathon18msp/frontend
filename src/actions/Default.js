import {
  API_START, API_SUCCESS,
  API_FAIL, ADD_USER_SUCCESS,
  CLICK_ON_SKILL_ITEM, CLICK_ON_WISHLIST_ITEM, GET_USER_SKILLS_SUCCESS, GET_ALL_SKILLS_SUCCESS, SKILLS_AND_WISH_SUCCESS,
  GET_ALL_USERS_WITH_SKILLS
} from './Constants';
import axios from 'axios';

export const
  URL_PREFIX = 'http://localhost:8080',
  addUser = user => dispatch => {
    dispatch({ type: API_START });
    axios.post(`${URL_PREFIX}/user`, user)
      .then(response => {
        dispatch({
          payload: response.data,
          type: ADD_USER_SUCCESS
        });
      })
      .catch(error => dispatch({
        payload: error,
        type: API_FAIL
      }));

    return {};
  },
  getSkills = name => dispatch => {
    dispatch({ type: API_START });
    axios.get(`${URL_PREFIX}/skill`)
      .then(response => {
        const _ = {};
        _[name] = response.data;
        dispatch({
          payload: _, // array of objects id type and name
          type: GET_ALL_SKILLS_SUCCESS
        });
      })
      .catch(error => dispatch({
        payload: error,
        type: API_FAIL
      }));

    return {};
  },
  updateSkillsAndWishlist = updateObject => dispatch => {
    dispatch({ type: API_START });
    const promises = [
      axios.put(`http://localhost:8080/user/${updateObject.id}/skill`, updateObject.skills),
      axios.put(updateObject.updateWishlistHref, updateObject.wishlist)
    ];
    Promise.all(promises)
      .then(() => {
        dispatch({
          type: SKILLS_AND_WISH_SUCCESS
        })
          .catch(error => dispatch({
            payload: error,
            type: API_FAIL
          }));

        return {};
      });
  },
  updateUser = (updateHref, user) => dispatch => {
    dispatch({ type: API_START });
    axios.put(updateHref, user)
      .then(response => {
        // dispatch({
        //   payload: [response.data],
        //   type: API_SUCCESS
        // });
      })
      .catch(error => dispatch({
        payload: error,
        type: API_FAIL
      }));

    return {};
  },
  getUser = (skillsHref, interestsHref, selfHref) => dispatch => {
    dispatch({ type: API_START });
    const promises = [
      axios.get(skillsHref),
      axios.get(interestsHref),
      axios.get(selfHref)
    ];
    Promise.all(promises)
      .then(
        (
          [
            skills,
            interests,
            self
          ]) => {
          dispatch({
            payload: [
              skills,
              interests,
              self
            ],
            type: API_SUCCESS
          });
        })
      .catch(error => dispatch({
        payload: error,
        type: API_FAIL
      }));

    return {};
  },
  getUsers = () => dispatch => {
    dispatch({ type: API_START });
    axios.get(`${URL_PREFIX}/user`)
      .then(response => dispatch({
        payload: response.data,
        type: API_SUCCESS
      }))
      .catch(error => dispatch({
        payload: error,
        type: API_FAIL
      }));

    return {};
  },
  getUsersWithData = () => dispatch => {
    dispatch({ type: API_START });
    axios.get(`${URL_PREFIX}/user`)
      .then(users => {
        Promise.all(users.data.map(x => axios.get(x.links.find(link => link.rel === 'getUserSkills').href))).then(args => {
          const usersWithData = {},
            data = users.data.map(user => ({
              ...user,
              skills: usersWithData[user.id] || []
            }));
          args.filter(x => x.data.length).forEach(x => usersWithData[x.data[0].userId] = x.data);
          console.log(data, usersWithData);
          dispatch({
            type: GET_ALL_USERS_WITH_SKILLS,
            payload: data
          });
        })
          .catch(console.log);
      })
      .catch(console.log);

    return {};
  },
  selectSkillItem = payload => ({
    payload,
    type: CLICK_ON_SKILL_ITEM
  }),
  selectWantedItem = payload => ({
    payload,
    type: CLICK_ON_WISHLIST_ITEM
  }),
  getUserSkills = id => dispatch => {
    dispatch({ type: API_START });

    axios.get(`${URL_PREFIX}/skill`)
      .then(skills => {
        axios.get(`${URL_PREFIX}/user/${id}/skill`)
          .then(existing => {
            existing.data.forEach(existing => {
              const temp = skills.data.find(x => x.id === existing.skillId);
              if (temp) {
                console.log(temp);
                temp.selected = true;
              }
            });
            dispatch({
              payload: skills.data,
              type: GET_USER_SKILLS_SUCCESS
            });
          })
          .catch(error => dispatch({
            payload: error,
            type: API_FAIL
          }));
      })
      .catch(error => dispatch({
        payload: error,
        type: API_FAIL
      }));


    return {};
  };
