import { createMuiTheme } from '@material-ui/core';
import green from '@material-ui/core/colors/green';

const CustomTheme = createMuiTheme({
  palette: {
    primary: green,
    secondary: {
      main: '#8bc34a'
    }
  }
});

export default CustomTheme;