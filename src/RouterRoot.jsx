import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import test from './components/Test';
import CreateUser from './components/Profile/CreateUser';
import UpdateUser from './components/Profile/UpdateUser';
import Index from './components/Profile/Index';
import NavBar from './components/NavBar';
import Button from '@material-ui/core/Button';
import Users from './components/Users';
import AllSkills from './components/AllSkills';


class RouterRoot extends Component {

  render() {
    return (
      <div>
        <BrowserRouter>
          <div>
            <NavBar />
            <br />
            <Switch>
              <Route exact path="/" component={this.home} />
              <Route path="/users" component={Users} />
              <Route path="/allskills" component={AllSkills} />
              <Route path="/test" component={test} />
              <Route exact path="/profile/new" component={CreateUser} />
              <Route path="/profile/:id" component={UpdateUser} />
              <Route exact path="/profile" component={Index} />
              <Route component={this.notFound} />
            </Switch>
          </div>
        </BrowserRouter>
      </div>
    );
  }

  home = props => {
    const navigate = url => () => props.history.push(url);

    return (
      < div >
        <br />
        <h3>New to Daugherty...?</h3>
        <Button onClick={navigate('/profile/new')} variant="contained" color="secondary">Add a Consultant</Button>
        <br />
        <h3>Let us know what you can do...</h3>
        <Button onClick={navigate('/profile/1')} variant="contained" color="secondary">Add Skillset</Button>
        <br />
        <h3>Looking for someone to put on an engagement...?</h3>
        <Button onClick={navigate('/profile')} variant="contained" color="secondary">Find Consultant by Skillset</Button>
      </div >
    );
  }

  stuff = () => (
    <marquee>this is stuffs! :)</marquee>
  );

  users = props => (
    <button onClick={() => props.history.push('/users')}>View All Users</button>
  );

  notFound = () => (
    <marquee>NOT FOUND!!! 404 NOT FOUND!! 404 NOT FOUND! 404</marquee>
  )
}

export default RouterRoot;