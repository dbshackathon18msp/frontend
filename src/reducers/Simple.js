import {
  EXAMPLE,
  API_FAIL,
  API_START,
  API_SUCCESS,
  ADD_USER_SUCCESS,
  GET_SKILLS_SUCCESS,
  UPDATE_SKILLS_SUCCESS,
  CLICK_ON_SKILL_ITEM, CLICK_ON_WISHLIST_ITEM,
  GET_ALL_SKILLS_SUCCESS,
  GET_USER_SKILLS_SUCCESS,
  GET_ALL_USERS_WITH_SKILLS
} from '../actions/Constants';

const initialState = {};

// eslint-disable-next-line max-lines-per-function
export default (state = initialState, action) => {
  switch (action.type) {
    case EXAMPLE:
      return {
        ...state,
        example: action.payload
      };
    case API_FAIL:
      return {
        ...state,
        error: action.payload.toString(),
        status: 'error'
      };
    case API_START:
      return {
        ...state,
        error: null,
        status: 'started'
      };
    case API_SUCCESS:
      return {
        ...state,
        allUsers: action.payload,
        status: 'success'
      };
    case CLICK_ON_SKILL_ITEM:
      return {
        ...state,
        skills: [...action.payload]
      };
    case CLICK_ON_WISHLIST_ITEM:
      return {
        ...state,
        wanted: { ...action.payload }
      };
    case ADD_USER_SUCCESS:
      return {
        ...state,
        user: action.payload,
        status: 'success'
      };
    case GET_ALL_SKILLS_SUCCESS:
      return {
        ...state,
        ...action.payload,
        status: 'success'
      };
    case GET_SKILLS_SUCCESS:

      return {
        ...state,
        ...action.payload.data,
        status: 'success'
      };
    case UPDATE_SKILLS_SUCCESS:
      return {
        ...state,
        skills: action.payload,
        status: 'success'
      };
    case GET_USER_SKILLS_SUCCESS:
      return {
        ...state,
        existingSkills: action.payload
      };
    case GET_ALL_USERS_WITH_SKILLS:
      return {
        ...state,
        allUsersWithData: action.payload
      };
    default:
      return state;
  }
};