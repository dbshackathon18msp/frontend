import { createStore, applyMiddleware, compose } from 'redux';
import reducers from '../reducers/Reducers';
import thunk from 'redux-thunk';

export const store = createStore(
  reducers,
  compose(
    applyMiddleware(thunk),
    window.devToolsExtension ? window.devToolsExtension()
      : f => f)
);